FROM node:16-alpine

# Création du dossier de travail
WORKDIR /app

# Copie du code source
COPY . .

# Installation des dépendances
RUN npm install --only=production

# Exposition du port
EXPOSE 8080

# Démarrage de l'application
CMD ["npm", "start"]
